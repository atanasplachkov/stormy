# Changelog #

### 15 Feb 2015 - 0.1.1 ###
- Setting a pair and retrieving should prepend to the LRU store instead of append.

### 12 Feb 2015 - 0.1.0 ###
- First revision
