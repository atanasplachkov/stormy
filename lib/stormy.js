"use strict";

var List = require("smart-list");

var DEFAULT_TTL = 1800;
var DEFAULT_CLEAN_INTERVAL = 900;
var MAX_ELEMENTS_TO_CLEAN = 1000;

var cleaner = function (cache) {
	var lru = cache._lru;
	var i = 0;
	var iterator = lru.last();
	var data;
	var now = Date.now();
	while (iterator && i < MAX_ELEMENTS_TO_CLEAN) {
		data = iterator.data;
		if (now > data.timestamp + data.expiration) {
			iterator.drop();
			delete cache._store[data.key];
			i++;
		}/* else {
			// We are done. They are sorted by access time.
			// TODO: This logic will be valid when there are different LRU lists for the different expiration times.
			break;
		}*/
		iterator = iterator.previous();
	}
};

var Stormy = function (options) {
	var _options = options;
	if (!_options) {
		_options = {};
	}

	this._expiration = (!isNaN(_options.expiration) ? _options.expiration : DEFAULT_TTL) * 1000;
	this._cleanInterval = (!isNaN(_options.cleanInterval) ? _options.cleanInterval : DEFAULT_CLEAN_INTERVAL) * 1000;

	this.clear();
};

Stormy.prototype.clear = function () {
	this._store = Object.create(null);
	if (!this._lru) {
		this._lru = new List();
	} else {
		this._lru.clear();
	}

	if (this._cleanInterval > 0) {
		setInterval(cleaner, this._cleanInterval, this).unref();
	}
};

Stormy.prototype.set = function (key, value, expiration) {
	var item = this._store[key];
	if (item) {
		item.drop();
	}

	var data = Object.create(null);
	data.expiration = (!isNaN(expiration) ? expiration * 1000 : this._expiration);
	data.timestamp = Date.now();
	data.key = key;
	data.value = value;

	this._store[key] = this._lru.unshift(data);
};

Stormy.prototype.get = function (key) {
	var item = this._store[key];
	if (!item) {
		return;
	}

	var data = item.data;
	if (Date.now() > data.timestamp + data.expiration) {
		item.drop();
		delete this._store[key];
		return;
	}

	item.drop();
	data.timestamp = Date.now();
	this._store[key] = this._lru.unshift(data);

	return data.value;
};

Stormy.prototype.has = function (key) {
	return (this._store[key] !== void 0);
};

Stormy.prototype.del = function (key) {
	var item = this._store[key];
	if (!item) {
		return;
	}

	var value = item.data.value;
	item.drop();
	delete this._store[key];

	return value;
};

Stormy.prototype.mset = function (items) {
	var i;
	var len = items.length;
	var item;

	for (i = 0; i < len; i++) {
		item = items[i];

		this.set(items.key, items.value, items.expiration);
	}
};

Stormy.prototype.mget = function (items) {
	var i;
	var len = items.length;
	var result = [];

	for (i = 0; i < len; i++) {
		result.push(this.get(items[i]));
	}

	return result;
};

Stormy.prototype.mdel = function (items) {
	var i;
	var len = items.length;

	for (i = 0; i < len; i++) {
		this.del(items[i]);
	}
};

module.exports = Stormy;