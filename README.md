# Stormy - the cache you need #

Highly optimized memory cache with LRU cleaning policy combined with expiration times.

## Installation ##

```
#!js

npm install stormy

```

## Example usage ##

```
#!js

var Stormy = require("stormy");
var stormy = new Stormy();

stormy.set("foo", "bar");
stormy.get("foo");
```

## Reference ##

### Constructor properties ###

The constructor accepts **options** object with the following keys:

- **expiration** - default expiration time to be used for every item, if it is not explicitly specified when using **set**. Default is *1800* (in seconds).
- **cleanInterval** - how often the cache should be checked for expired values. Default is *900* (in seconds).

### Simple methods ###

- **set** - function (key, value, *optional* expiration)
Add a new element to the cache, mapping it to the specified **key**. It overwrites an existing value with that key. **expiration** specifies how much time (in **seconds**), the value should be kept in the cache.

- **get** - function (key)  
Retrieves an element. Returns **undefined**, if the element was not found or the value.

- **has** - function (key)
Returns true, if an item with that key exists. This method does NOT touch the expiration time as in **get**.

- **del** - function (key)  
Deletes an element, while also returning its value, if it was found.

- **clear** - function ()  
Invalidates the cache (deletes everything in it).

### Multi-data methods ###

- **mset** - function (array)  
Accepts an array of objects. Each object must have **key** and **value**, while **expiration** is optional.

- **mget** - function (array)  
Accepts an array of strings. Returns an array with the values.

- **mdel** - function (array)  
Accepts an array of strings.

# LICENSE #
The MIT License (MIT)

Copyright (c) 2015 Atanas Plachkov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.