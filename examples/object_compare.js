"use strict";

var stormy = new (require("../"))();
var obj = Object.create(null);

var letters = "abcdefghijklmnopqrstuvwxyz";
var N = 1000000;
var time, i, res;
var keys = [];

var randomString = function () {
	var i, str = "";
	for (i = 0; i < 6; i++) {
		str += letters[Math.floor(Math.random() * letters.length)];
	}
	return str;
};

// Fill up
time = Date.now();
for (i = 0; i < N; i++) {
	var str = randomString();
	stormy.set(str, i);
	obj[str] = i;

	if (i % 1000 === 0) {
		keys.push(str);
	}
}
console.log("Fill-up time (ms): " + (Date.now() - time));

N = keys.length;
time = Date.now();
for (i = 0; i < N; i++) {
	res = stormy.get(keys[i]);
}
console.log("Search time Stormy (ms): " + (Date.now() - time));

time = Date.now();
for (i = 0; i < N; i++) {
	res = obj[keys[i]];
}
console.log("Search time Object (ms): " + (Date.now() - time));